package com.example.web;

import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CustomerForm {
	@NotNull	//入力チェックのアノテーション
	@Size(min=1, max=127)	//文字列長
	private String firstName;
	@NotNull
	@Size(min=1, max=127)
	private String lastName;
}
